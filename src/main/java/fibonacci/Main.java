package fibonacci;

/**
 * Main.java - a  class for demonstrating the how program works.
 * @author  Lidia Fedorova
 * @version 1.0
 * @see Main
 */
public  class Main {
    /**
     * @param args - Argument
     * @see Main#main(String[])
     */
    public static void main(String[] args) {
/**
 * @param menu - Menu object
 */
        Menu menu=new Menu();
        /**
         * Here is start point of the program
         */
        menu.startPO();
    }
}


