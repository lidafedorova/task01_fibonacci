package fibonacci;

final class Constants {

    public static final int PERCENT = 100;
    public static final String ODD="the sum of odd numbers ";
    public static final String SUMODD="the sum of odd numbers";
    public static final String MAXODD="the biggest odd number(F1)";
    public static final String MAXEVEN="the biggest even number(F2)";
    public static final String PERCENTODD = "percentage of odd Fibonacci numbers";
    public static final String PERCENTEVEN = " percentage of odd Fibonacci numbers";
    public static final String X1= "Enter x1:";
    public static final String X2= "Enter x2:";
    public static final String SELECT2="2. Print odd numbers from start to the end";
    public static final String SELECT3="3. Print odd numbers from end to start" ;
    public static final String SELECT4="4. Print the sum of odd numbers";
    public static final String SELECT5="5. Print the sum of even numbers";
    public static final String SELECT6="6. Please enter n(Fibonacci numbers)";
    public static final String SELECT7="7. Find the biggest odd number(F1)";
    public static final String SELECT8="8. Find the biggest even number(F2)";
    public static final String SELECT9="9. Print percentage of odd Fibonacci numbers";
    public static final String SELECT10="10. Print percentage of even Fibonacci numbers";
    public static final String SELECT12="12. Please enter to exit";
    public static final String SELECT1="1.Please enter the interval" ;
    public static final String SELECT11="11. Fibonacci numbers";
    public static final String SELECT="Select a menu item:";


}
