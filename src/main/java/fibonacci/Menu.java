package fibonacci;

/**
 * Menu.java - a  class for demonstrating the view of the project
 * @author  Lidia Fedorova
 * @version 1.0
 * @see Menu
 */
import java.util.Scanner;
import java.util.*;
public  class Menu {
    int x1=0;
    int x2=0;
    int N=0;
    public  void startPO() {
        /**
         * @param userInput- Scanner object
         */
        Scanner userInput = new Scanner(System.in);
        /**
         * @param calculations - Calculations object
         */

        Calculations calculations=new Calculations();

        boolean exit = false;
        while (!exit) {

            /**
             * @param args - Argument
             * @see Result#main(String[])
             */
            programMenu();
            int menuinput = userInput.nextInt();
            if (menuinput == 1) {

                System.out.println(Constants.X1);
                x1=userInput.nextInt();
                System.out.println(Constants.X2);
                x2=userInput.nextInt();
            } else if (menuinput == 2) {
                List<Integer> integers=calculations.generateNum(x1,x2);
                calculations.showList(calculations.OddNumbers(integers));
            } else if (menuinput == 3) {
                List<Integer> integers=calculations.generateNumDesc(x1,x2);
                calculations.showList(calculations.evenNumbers(integers));
            } else if (menuinput == 4) {
                double sum=calculations.sum(calculations.OddNumbers(calculations.generateNumDesc(x1,x2)));
                System.out.println(Constants.ODD+sum);
            }else if (menuinput == 5) {
                double sum=calculations.sum(calculations.evenNumbers(calculations.generateNumDesc(x1,x2)));
                System.out.println(Constants.SUMODD+sum);
            }else if (menuinput == 6) {
                N=userInput.nextInt();
            }else if (menuinput == 7) {
                List<Integer>fibonacci=calculations.fibonacci(N);
                int N=calculations.biggestOddNum(fibonacci);
                System.out.println(Constants.MAXODD+ N );
            }else if (menuinput == 8) {
                List<Integer>fib=calculations.fibonacci(N);
                int N=calculations.biggestEvenNum(fib);
                System.out.println(Constants.MAXEVEN+ N );
            }else if (menuinput == 9) {
                List<Integer>fibos=calculations.fibonacci(N);
                System.out.println(Constants.PERCENTODD+ calculations.findPercentNotEven(fibos)+" %");
            }else if (menuinput == 10) {
                List<Integer>fibos=calculations.fibonacci(N);
                System.out.println(Constants.PERCENTEVEN+ calculations.findPercentEven(fibos)+" %");
            } else if (menuinput == 11) {
                List<Integer>fibos=calculations.fibonacci(N);

            }
            else if (menuinput == 12) {
                exit = true;
            }
        }
    }
    private static  void programMenu() {
        System.out.println(
                Constants.SELECT + "\n"

                        + Constants.SELECT1 + "\n"
                        + Constants.SELECT2 + "\n"
                        + Constants.SELECT3 + "\n"
                        + Constants.SELECT4 + "\n"
                        + Constants.SELECT5 + "\n"
                        + Constants.SELECT6 + "\n"
                        + Constants.SELECT7 + "\n"
                        + Constants.SELECT8 + "\n"
                        + Constants.SELECT9 + "\n"
                        + Constants.SELECT10 + "\n"
                        + Constants.SELECT11 + "\n"
                        + Constants.SELECT12 + "\n"

        );
    }
}













