package fibonacci;

/**
 * Calculation.java - a simple class for demonstrating the calculations od Fibonacci numbers.
 * @author  Lidia Fedorova
 * @version 1.0
 * @see Calculations
 */
import java.util.*;


public class Calculations {

    /**
     * @see Calculations#showList(List)
     * @param integers - Integers
     */
    public void showList(List<Integer> integers) {

        /**
         * @param in - Argument
         */ for (Integer in : integers) {
            System.out.println(in);
        }
    }
    /**
     * @return int generate numbers in interval from  start to end
     * @see Calculations#generateNum(int, int)
     * @param x1 - first number
     * @param x2 - last number
     */
    public List<Integer> generateNum(int x1, int x2) {
        List<Integer> integers = new ArrayList<Integer>();
        for (int i = x1; i <= x2; i++) {
            integers.add(i);
        }
        return integers;
    }
    /**
     * @return int generate numbers in interval from end to start
     * @param x1 - first number
     * @param x2 - last number

     * @see Calculations#generateNumDesc(int, int)
     */
    public List<Integer> generateNumDesc(int x1, int x2) {
        /**
         * @param integers -all numbers
         */
        List<Integer> integers = new ArrayList<Integer>();
        for (int i = x2; i >= x1; i--) {
            integers.add(i);
        }
        return integers;
    }
    /**
     * @return int even numbers in interval
     * @see Calculations#evenNumbers(List)
     * @param integers -all numbers
     */
    public List<Integer> evenNumbers(List<Integer> integers) {

        List<Integer> integerList = new ArrayList<Integer>();

        for (Integer in : integers) {
            if (in % 2 == 0) {
                integerList.add(in);
            }
        }
        return integerList;
    }
    /**
     * @return int odd numbers  in interval
     * @param integers -all numbers
     * @see Calculations#OddNumbers(List)
     */

    public List<Integer> OddNumbers(List<Integer> integers) {
        /**
         * @param integerList- ArrayList object

         */

        List<Integer> integerList = new ArrayList<Integer>();

        for (Integer in : integers) {
            if (in % 2 != 0) {
                integerList.add(in);
            }
        }
        return integerList;
    }
    /**
     * @return int sum of  numbers in interval
     * @see Calculations#sum(List)
     * @param integers -all numbers
     */
    public Integer sum(List<Integer> integers) {

        int sum = 0;
        for (Integer number : integers) {
            sum += number;
        }
        return sum;
    }
    /**
     * @return int integers of  fibonacci numbers in interval
     * @see Calculations#fibonacci(Integer) ()
     * @param integer- ArrayList object
     */
    public List<Integer> fibonacci(Integer integer) {

        List<Integer> integers = new ArrayList<Integer>();
        int a = 1, b = 1;
        int f = 0;
        for (int i = 0; i < integer; i++) {
            f = a + b;
            a = b;
            b = f;
            integers.add(f);
            System.out.print(f + " ");

        }
        return integers;
    }
    /**
     * @return int the biggest Even number of all numbers in interval
     * @see Calculations#biggestEvenNum(List)
     * @param integers -all numbers
     */
    public Integer biggestEvenNum(List<Integer> integers) {

        int N = 0;
        for (int i = integers.size() - 1; i >= 0; i--) {
            if (integers.get(i) % 2 == 0) {
                N = integers.get(i);
                break;
            }
        }
        return N;
    }
    /**
     * @return int the biggest Odd number of all numbers in interval
     * @see Calculations#biggestOddNum(List)
     * @param integers -all numbers
     */
    public Integer biggestOddNum(List<Integer> integers) {
        int N = 0;
        for (int i = integers.size() - 1; i >= 0; i--) {
            if (integers.get(i) % 2 != 0) {
                N = integers.get(i);
                break;
            }
        }
        return N;
    }
    /**
     * @return double percent of even numbers in interval
     * @see Calculations#findPercentEven(List)
     * @param integers -all numbers
     */
    public double findPercentEven(List<Integer> integers) {
        return (evenNumbers(integers).size() / integers.size()) * Constants.PERCENT;
    }

    /**
     * @return double percent of odd numbers in interval
     * @see Calculations#findPercentNotEven(List)
     * @param integers -all numbers
     */
    public double findPercentNotEven(List<Integer> integers) {
        return (OddNumbers(integers).size() / integers.size()) * Constants.PERCENT;
    }
}


